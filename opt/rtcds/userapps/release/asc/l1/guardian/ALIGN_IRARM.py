# Docstring:
"""
Inital Alignment Guardian: Infrared Arm Alignment.

This guardian script is designed to align the IR input beam to the X arm or Y arm. 
It is intended to be run after the ALIGN_XARM, and ALIGN_YARM guardians.

Author: Nathan Holland, A. Mullavey.
Date: 2019-05-13
Contact: nathan.holland@ligo.org

Modified: 2019-05-13 (Created).
Modified: 2019-05-15 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-16 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-17 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-20 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-21 (Some clean up, removing path changes).
Modified: 2019-05-22 (Changed structure to make it more self contained).
Modified: 2019-05-24 (Syntatic changes, debugging from lower level API).
Modified: 2019-05-31 (Added INIT state).
Modified: 2019-06-07 (Fixed ezca feedthrough for align_restore, and align_save
                      functions; Made use of align_restore, and align_save
                      functions; Move towards PEP8 style).
Modified: 2019-06-12 (Added notifications; rework to do list).
Modified: 2019-06-17 (Added dictionaries, with placeholder values, for use in
                      better stop condition determination.).
Modified: 2019-06-20 (Added data logging, for testing purposes; and the
                      gradient tracker.)
Modified: 2019-07-01 (Added data collection).
Modified: 2019-07-02 (Testing and debugging).
Modified: 2024-03-19 (Renamed and made this a common for X and Y).
"""


#------------------------------------------------------------------------------
# Imports:
# Import time, needed for the sleep function.
import time

# Import the simple namespace class from argparse.
from argparse import Namespace

# Import the minimum number of needed guardian subutilities.
from guardian import GuardState, GuardStateDecorator, Node

# Import the class Optic from optic.py
# Source @
# /opt/rtcds/userapps/release/asc/l1/guardian/optic.py
from optic import Optic

# Import the class TrigServo from trigservo.py.
# Source @
# /opt/rtcds/userapps/release/asc/l1/guardian/trigservo.py
from trigservo import TrigServo

# Import the LSC matrices access.
# Source @
# /opt/rtcds/userapps/release/isc/l1/guardian/isclib/matrices.py
import isclib.matrices as matrix

# Import the function align_restore from new_align_restore.py.
# Source @
# /opt/rtcds/userapps/release/sus/common/scripts/new_align_restore.py
from new_align_restore import align_restore

# Import the function align_save from new_align_save.py.
# Source @
# /opt/rtcds/userapps/release/sus/common/scripts/align_save.py
from new_align_save import align_save

# Import the class Tracker from
# /opt/rtcds/userapps/release/asc/l1/guardian/gradient_tracker.py
from gradient_tracker import Tracker

# Import the EzAvg class from
# /opt/rtcds/userapps/release/isc/l1/guardian/isclib/epics_averaging.py
from isclib.epics_average import EzAvg, ezavg

import numpy as np

import cdsutils


#------------------------------------------------------------------------------

arm_locked_threshold = 15

#------------------------------------------------------------------------------
# Script Variables:

# Define the nominal state for this guardian script.
nominal = "IR_ALIGN_IDLE"
request = nominal

# Convenient access for ITMX.
itmx = Optic("ITMX")
etmx = Optic("ETMX")
itmy = Optic("ITMY")
etmy = Optic("ETMY")
prm = Optic("PRM")
srm = Optic("SRM")

# Namespace defining the operating points for IRX locking.
_irx_lock = {"err_set" : 0,     # The error signal set point.
             "err_dev" : None,  # The allowable error signal deviation.
             "trans_min" : 0,   # Manual alignment transmitted cut off.
             "trans_alw" : None # Minimum allowable transmitted value.
            }
irx_lock = Namespace(**_irx_lock)


# Namespace defining the operating points for PR2 pitch alignment.
_pr2_p = {"err_set" : 0,     # The error signal set point.
          "err_dev" : None,  # The allowable error signal deviation.
          "trans_min" : 0,   # Manual alignment transmitted cut off.
          "trans_alw" : None # Minimum allowable transmitted value.
         }
pr2_p = Namespace(**_pr2_p)

# Namespace defining the operating points for PR2 yaw alignment.
_p2r_y = {"err_set" : 0,     # The error signal set point.
          "err_dev" : None,  # The allowable error signal deviation.
          "trans_min" : 0,   # Manual alignment transmitted cut off.
          "trans_alw" : None # Minimum allowable transmitted value.
         }
p2r_y = Namespace(**_p2r_y)


# Namespace defining the operating points for IM4 pitch alignment.
_im4_p = {"err_set" : 0,     # The error signal set point.
          "err_dev" : None,  # The allowable error signal deviation.
          "trans_min" : 0,   # Manual alignment transmitted cut off.
          "trans_alw" : None # Minimum allowable transmitted value.
         }
im4_p = Namespace(**_im4_p)

# Namespace defining the operating points for IM4 yaw alignment.
_im4_y = {"err_set" : 0,     # The error signal set point.
          "err_dev" : None,  # The allowable error signal deviation.
          "trans_min" : 0,   # Manual alignment transmitted cut off.
          "trans_alw" : None # Minimum allowable transmitted value.
         }
im4_y = Namespace(**_im4_y)

#------------------------------------------------------------------------------
# Nodes to manage
imc_node = Node("IMC_LOCK")
alsx_node = Node("ALS_XARM")
alsy_node = Node("ALS_YARM")

#------------------------------------------------------------------------------
# Parameters

xarm_locked_threshold = 15
yarm_locked_threshold = 15

#------------------------------------------------------------------------------
# Functions

def IMC_10W():
    return (imc_node.arrived and (ezca['IMC-IM4_TRANS_SUM_OUTPUT']>9) and (imc_node.state == 'LOCKED_10W'))

'''
def IMC_lock_state():
    up_states = [""]
    powerUp_states = ["LOCKED_1W","GO_TO_LOCKED_10W"]
    if any(state = imc_node.state for state in powerUp_states):
        return "IMC Powering up"
    if any(state = imc_node.state for state in up_states):
        return "IMC Powering up
    elif ezca['IMC-IM4_TRANS_SUM_OUTPUT']<0.8
        return "IMC not locked"
'''

def ARM_locked():
    p_trans = ezca['LSC-TR_{}_NORM_INMON'.format(arm)]
    return p_trans >= arm_locked_threshold

def turn_off_wfs():
    ezca.switch('ASC-INP2_P','INPUT','OFF')
    ezca.switch('ASC-INP2_Y','INPUT','OFF')
    ezca.switch('ASC-PRC2_P','INPUT','OFF')
    ezca.switch('ASC-PRC2_Y','INPUT','OFF')
    ezca.switch('ASC-INP1_P','INPUT','OFF')
    ezca.switch('ASC-INP1_Y','INPUT','OFF')
    # clear histories?
    #time.sleep(0.5)

def clear_wfs_history():
    for dof1 in ['INP2','PRC2','INP1']:
        for dof2 in ['P','Y']:
            ezca['ASC-'+dof1+'_'+dof2+'_RSET'] = 2
    time.sleep(0.1)
    ezca['SUS-PR2_M1_LOCK_P_RSET'] = 2
    ezca['SUS-PR2_M1_LOCK_Y_RSET'] = 2

def clear_dc_centering_history():
    for dc in ['DC1','DC2']:
        for dof in ['P','Y']:
            ezca['ASC-'+dc+'_'+dof+'_RSET'] = 2

def turn_on_dc_centering():
    ezca.switch(":ASC-DC1_P", "INPUT", "ON")
    ezca.switch(":ASC-DC1_Y", "INPUT", "ON")
    ezca.switch(":ASC-DC2_P", "INPUT", "ON")
    ezca.switch(":ASC-DC2_Y", "INPUT", "ON")


def turn_off_dc_centering():
    ezca.switch(":ASC-DC1_P", "INPUT", "OFF")
    ezca.switch(":ASC-DC1_Y", "INPUT", "OFF")
    ezca.switch(":ASC-DC2_P", "INPUT", "OFF")
    ezca.switch(":ASC-DC2_Y", "INPUT", "OFF")

# note that we use YARM back for both X and Y locking
def set_arm_lock():
    ezca['LSC-YARM_TRAMP'] = 0
    matrix.lsc_trigger.zero(row='YARM')
    matrix.lsc_input.zero(row='YARM')
    matrix.lsc_input['YARM','ASVAC_A_RF45_I'] = 1
    matrix.lsc_input.TRAMP = 0    
    time.sleep(0.1)
    matrix.lsc_input.load()
    matrix.lsc_pow_norm['YARM', 'TRY_A_LF'] = 0
    ezca['LSC-YARM_GAIN'] = -6	#-3 #ajm180312
    ezca.switch('LSC-YARM', 'FM2', 'FM4', 'FM5', 'FM6', 'INPUT', 'OUTPUT', 'ON')
 
def unlock_arm():
    ezca['LSC-YARM_TRIG_THRESH_OFF'] = -10
    ezca['LSC-YARM_FM_TRIG_THRESH_OFF'] = -10
    ezca['LSC-YARM_TRAMP'] = 3
    time.sleep(1)
    ezca['LSC-YARM_GAIN'] = 0
    time.sleep(4)   
    ezca.switch('LSC-YARM', 'OUTPUT', 'OFF')
    ezca['LSC-YARM_TRIG_THRESH_OFF'] = 3
    ezca['LSC-YARM_FM_TRIG_THRESH_OFF'] = 3

#------------------------------------------------------------------------------
# Guardian Decorators

class assert_imc_10W(GuardStateDecorator):
    def pre_exec(self):
        if not IMC_10W():
            unlock_arm()
            return 'SET_POWER_TO_10W'

class assert_arm_locked(GuardStateDecorator):
    def pre_exec(self):
        if not ARM_locked():
            turn_off_wfs()
            turn_off_dc_centering()
            return 'LOCK_IR_CAV'

#------------------------------------------------------------------------------
# TODO:

# Control IMC and set to 10W (although maybe should be done earlier)
# Watch for IMC Locklosses


#------------------------------------------------------------------------------
#                                 Guardian States
#------------------------------------------------------------------------------

# Initial state - Always defined, or added if missing.
class INIT(GuardState):
    request = False
    index = 0
    
    # Main class method - Does nothing.
    def main(self):
        # Succeeds.
        return True

#------------------------------------------------------------------------------
# Idle state - Intentionally does nothing, allows for recovery.
class IR_ALIGN_IDLE(GuardState):
    request = True 
    index = 2
    
    # Main class method - Sets a timer for 1 second.
    def main(self):
        # Set a 1 second timer.
        self.timer["wait"] = 1
    
    # Run class method - Waits for timer to expire.    
    def run(self):
        # Check for timer expiration.
        if self.timer["wait"]:
            # Timer has expired, so succeed.
            return True

#------------------------------------------------------------------------------
# Reset state - Resets all of the changes to filters, matrices and
# optics done in this guardian.
class IR_ALIGN_RESET(GuardState):
    goto = True
    request = True
    index = 1
    
    # Main class method - Reset.
    def main(self):

        turn_off_wfs()
        turn_off_dc_centering()

        # Gracefully unlock the arm
        unlock_arm()

        clear_wfs_history()
        clear_dc_centering_history()

        # Return IMC L gain
        ezca['IMC-L_GAIN'] = 1.0

        # Reset all of the misaligned optics.
#        itmy.align('Y', ezca)
#        etmy.align('P', ezca)
#        prm.align('P', ezca)
#        prm.align('Y', ezca)
#        srm.align('P', ezca)
#        align_restore("PR2", ezca)
#        align_restore("IM4", ezca)

        # Block the fast shutter.
        #ezca.write(":SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK", 0)

        # Reset the LSC trigger matrix.
        matrix.lsc_trigger["YARM", "TRX_A_LF"] = 0

        # Reset the power normalisation matrix.
        matrix.lsc_pow_norm["YARM", "TRY_A_LF"] = 1

        # Reset the LSC input matrix, as per IFO DOWN (ISC_LOCK
        # guardian)
        matrix.lsc_input.zero(row = "YARM")
        matrix.lsc_input.TRAMP = 0
        matrix.lsc_input.load()

        # Switch off the REFL WFS centring.
        ezca.switch(":ASC-DC1_P", "INPUT", "OFF")
        ezca.switch(":ASC-DC1_Y", "INPUT", "OFF")
        ezca.switch(":ASC-DC2_P", "INPUT", "OFF")
        ezca.switch(":ASC-DC2_Y", "INPUT", "OFF")

        # Reset Y ARM filter module.
        ezca.switch(":LSC-YARM", "INPUT", "FM2", "FM4", "FM6", "OUTPUT", "OFF")

        # Load the altered LSC input matrix.
        matrix.lsc_input.load()

        # Set a timer, to wait for changes to take effect.
        self.timer["wait"] = 5

    # Run class method - Wait for the timer to expire.
    def run(self):
        # Check for timer expiration.
        if self.timer["wait"]:
            # Succeed.
            return True

#------------------------------------------------------------------------------
# Align IM3 state

class IM4_TRANS_CENTERING(GuardState):
    request = False
    index = 4

    # Main class method - Set up and engage IM4 trans cnetering servo
    def main(self):

        for dof in ['P','Y']:
            ezca.switch('ASC-INP1_{}'.format(dof),'INPUT','FMALL','OFF')
            ezca.switch('ASC-INP1_{}'.format(dof),'FM1','FM2','OUTPUT','ON')
            ezca['ASC-INP1_{}_RSET'.format(dof)] = 2
            ezca['ASC-INP1_{}_TRAMP'.format(dof)] = 0
            time.sleep(0.1)

        #ezca['ASC-INP1_P_GAIN'] = 0.5
        #ezca['ASC-INP1_Y_GAIN'] = 0.5

        #Set matrices
        matrix.asc_input_pit.zero(row = 'INP1')
        matrix.asc_input_yaw.zero(row = 'INP1')
        matrix.asc_output_pit.zero(col = 'INP1')
        matrix.asc_output_yaw.zero(col = 'INP1')
        time.sleep(0.1)
        matrix.asc_input_pit['INP1','IM4_TRANS'] = 1.0
        matrix.asc_input_yaw['INP1','IM4_TRANS'] = 1.0
        matrix.asc_output_pit['IM3','INP1'] = 1.0
        matrix.asc_output_yaw['IM3','INP1'] = 1.0
        
        ezca.switch('ASC-INP1_P','INPUT','ON')
        ezca.switch('ASC-INP1_Y','INPUT','ON')

        chans = ['IMC-IM4_TRANS_PIT_OUTPUT','IMC-IM4_TRANS_YAW_OUTPUT']
        self.im4_avgs = EzAvg(ezca,5,chans)


    # Run class method - Wait for IM4 trans QPD to be centered
    def run(self):

        [done,vals] = self.im4_avgs.ezAvg()
        if not done:
            return

        if any(val>0.01 for val in vals):
            notify('Waiting for IM4 to center')
            return

        return True

#------------------------------------------------------------------------------
# Misalignment state - Misalign optics so that only the X arm can be
# resonant.
class MISALIGN_FOR_IR_CAV(GuardState):
    request = True
    index = 5
    
    # Main class method - Misalign optics.
    def main(self):

        if arm == 'X':
            alsy_node.set_request("QPDS_LOCKED")
        elif arm == 'Y':
            alsx_node.set_request("QPDS_LOCKED")

        itmx.test_ramp('Y', 5, ezca)
        itmy.test_ramp('Y', 5, ezca)
        etmx.test_ramp('P', 5, ezca)
        etmy.test_ramp('P', 5, ezca)
        time.sleep(0.5)

        # Misalign PRM.
        prm.misalign('P', 1500, ezca)
        prm.misalign('Y', 1500, ezca)
        
        # Misalign SRM.
        srm.misalign('P', -1500, ezca)

        if arm == 'X':

            # Misalign ITMY, ETMY
            itmy.misalign('Y', -60, ezca)
            etmy.misalign('P', -40, ezca)

            # Align ITMX.
            itmx.align('P', ezca)
            itmx.align('Y', ezca)
        
            # Align ETMX.
            etmx.align('P', ezca)
            etmx.align('Y', ezca)

        elif arm == 'Y':

            # Misalign ITMX, ETMX
            itmx.misalign('Y', -60, ezca)
            etmx.misalign('P', -40, ezca)

            # Align ITMY.
            itmy.align('P', ezca)
            itmy.align('Y', ezca)
        
            # Align ETMY.
            etmy.align('P', ezca)
            etmy.align('Y', ezca)

        # Set a timer to wait for changes to take effect.
        self.timer["wait"] = 5

    # Run class method - Wait for changes to take effect.
    def run(self):
        # Check for timer expiration.
        if self.timer["wait"]:
            # Timer has expired so succeed.
            return True

#------------------------------------------------------------------------------
# Set Power to 10W if not already there
class SET_POWER_TO_10W(GuardState):
    request = False
    index = 7

    def main(self):

        # Need to do this to steal control?
        imc_node.set_managed()
        # Request 10W (maybe better to use IFO POWER state?)
        imc_node.set_request("LOCKED_10W")
        # Release control, no need to micromanage
        imc_node.release()

        # Set a timer to periodically check IMC status 
        self.timer['check'] = 2

        # A timer to tell the operator that IMC is taking too long
        self.timer['timeout'] = 120

    def run(self):

        #if self.timer['timeout']:
        #    notify("IMC_LOCKED guardian has failed to reach \"LOCKED_10W\" "+ \
        #           "within 120 seconds (timeout), please investigate.")

        notify("IMC LOCK currently in {} state".format(imc_node.state))

        if not self.timer['check']:
            return

        # Check every two seconds
        if not IMC_10W():
            self.timer['check'] = 2
            return

        return True

#------------------------------------------------------------------------------
# Lock Acquisition state - Lock the X arm cavity, with IR light.
class LOCK_IR_CAV(GuardState):
    request = False
    index = 9
    
    @assert_imc_10W
    def main(self):
        # Unblock the shutter.
        ezca.write(":SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK", 1)

        # Remove the ramp time.
        ezca.write(":LSC-YARM_TRAMP", 0)

        # Zero the LSC triggering matrix.
        matrix.lsc_trigger.zero(row='YARM')
        
        # Set the LSC input matrix.
        matrix.lsc_input.zero(row='YARM')
        matrix.lsc_input['YARM','ASVAC_A_RF45_I'] = 1
        matrix.lsc_input.TRAMP = 0
        #Sleep 0.1 seconds?
        matrix.lsc_input.load()

        # Zero the power normalisation.
        matrix.lsc_pow_norm['YARM', 'TRY_A_LF'] = 0

        # Set the filter module. 
        ezca.write(":LSC-YARM_GAIN", -6)
        ezca.switch("LSC-YARM", "INPUT", "FM2", "FM4", "FM5", "FM6", "OUTPUT",
                    "ON")

        # Reduce the IMC (MCL) gain.
        ezca['IMC-L_TRAMP'] = 1.0
        time.sleep(0.3)
        ezca['IMC-L_GAIN'] = 0.6
        time.sleep(0.3)

        # Enable appropriate triggering.
        matrix.lsc_trigger['YARM', 'TR{}_A_LF'.format(arm)] = 1

        # TODO: Any additional setup for lock acquisition monitoring.
        
        # Flag for the first iteration of run.
        self.first_run = True
        
        # For now just run this state for a fixed amount of time, 15 s.
        self.timer['wait'] = 10

        self.IrarmAvgs = EzAvg(ezca,2,'LSC-TR_{}_NORM_INMON'.format(arm))


    @assert_imc_10W
    def run(self):
        # TODO: Implement appropriate fault handling procedure.

        # Wait for cavity to lock.
        [done,power_trans] = self.IrarmAvgs.ezAvg()
        if not done:
            notify('Filling up EzAvg buffer')
            return

        #power_trans = cdsutils.avg(2, "LSC-TR_X_NORM_INMON")
        log("Power trans is {}".format(power_trans))
        if not (power_trans) > arm_locked_threshold:
            notify('Waiting on IR {}ARM to lock'.format(arm))
            return

        ## Check to see if this is the first pass of run.
        #if self.first_run:
        #    # Set the first run flag to false, for subsequent runs.
        #    self.first_run = False
        #    
        #    # Generate, and log, the header for the saved data.
        #    header = "TRANS, ERR"
        #    #log(header)
        #
        ## Format the data for the log.
        #data = "{0:.3e}, {1:.3e}".format(ezca.read(":LSC-TR_X_NORM_INMON"),
        #                                 ezca.read(":LSC-YARM_INMON"))
        
        # TODO: Implement appropriate monitoring of lock acquisition.
        # Check for sufficient transmitted power.
        #if ezca.read(":LSC-TR_X_NORM_INMON") > 10:
        #    # Succeed.
        #    return True
        
        # For now check for timer expiration.
        if not self.timer["wait"]:
            notify("Waiting for timer")
            return
            # Succeed.
        return True

#------------------------------------------------------------------------------
# X Arm Cavity Locked state - Monitor and ensure that the IR lock
# condition is satisfied.
class IR_CAV_LOCKED(GuardState):
    request = True
    index = 10
    
    # Main class method - Setup for lock monitoring.
    @assert_arm_locked
    def main(self):
        # TODO: Setupt for lock monitoring - similar to above.
        
        # Flag for the first iteration of run.
        self.first_run = True
        
        # For now just run this state for a fixed amount of time, 15 s.
        self.timer["wait"] = 5
    
    # Run class method - Monitor and ensure that the lock acquisition
    # is successful and complete.
    @assert_arm_locked
    def run(self):
        # TODO: Implement fault monitoring.

        # TODO: Implement check for lock acquisition completion.
        
        ## Check to see if this is the first pass of run.
        #if self.first_run:
        #    # Set the first run flag to false, for subsequent runs.
        #    self.first_run = False
        #    
        #    # Generate, and log, the header for the saved data.
        #    header = "TRANS, ERR"
        #    log(header)
        
        ## Format the data for the log.
        #data = "{0:.3e}, {1:.3e}".format(ezca.read(":LSC-TR_X_NORM_INMON"),
        #                                 ezca.read(":LSC-YARM_INMON"))
        
        # For the first 15 seconds.
        #if not self.timer["wait"]:
        #    # Save the information to the guardian log.
        #    log(data)
        
        # For now check for timer expiration.
        if self.timer["wait"]:
            # Succeed.
            return True

#------------------------------------------------------------------------------
# Center the beams on the WFS.
class REFLWFS_DC_CENTERING(GuardState):
    request = False
    index = 17

    # Main class method - Setup for the WFS feedback.
    # TODO: First check that there is light on diodes, or do this above
    @assert_arm_locked
    def main(self):

        #TODO: Check for light on REFL WFS, maybe do this before locking XARM

        # Switch on the WFS centering.
        turn_on_dc_centering()

        self.timer['wait'] = 5

    @assert_arm_locked
    def run(self):
        if not self.timer['wait']:
            return

        return True


#------------------------------------------------------------------------------
# Center the beams on the WFS.
class REFLWFS_DC_CENTERING_FOR_YARM(GuardState):
    request = False
    index = 37

    # Main class method - Setup for the WFS feedback.
    # TODO: First check that there is light on diodes, or do this above
    @assert_arm_locked
    def main(self):

        #TODO: Check for light on REFL WFS, maybe do this before locking XARM

        # Switch on the WFS centering.
        turn_on_dc_centering()

        self.timer['wait'] = 5

    @assert_arm_locked
    def run(self):
        if not self.timer['wait']:
            return

        return True

#------------------------------------------------------------------------------
class REFLWFS_TO_INPUT_SERVOS(GuardState):
    request = False
    index = 18

    #@assert_reflwfscentered
    @assert_arm_locked
    def main(self):

        # Set the input matrix
        #for dof in ['INP1','INP2','PRC2']:
        for dof in ['INP2','PRC2']:
            matrix.asc_input_pit.zero(row=dof)
            matrix.asc_input_yaw.zero(row=dof)

        matrix.asc_input_pit['INP2','REFL_A_RF9_I'] = 1
        matrix.asc_input_yaw['INP2','REFL_A_RF9_I'] = 1
        matrix.asc_input_pit['PRC2','REFL_B_RF9_I'] = 1
        matrix.asc_input_yaw['PRC2','REFL_B_RF9_I'] = 1
        #matrix.asc_input_pit['INP1','IM4_TRANS'] = 1
        #matrix.asc_input_yaw['INP1','IM4_TRANS'] = 1


        #Set the output matrices
        #for dof in ['INP1','INP2','PRC2']:
        for dof in ['INP2','PRC2']:
            matrix.asc_output_pit.zero(col=dof)
            matrix.asc_output_yaw.zero(col=dof)

        matrix.asc_output_pit['IM4','INP2'] = 1
        matrix.asc_output_yaw['IM4','INP2'] = 1
        matrix.asc_output_pit['PR2','PRC2'] = 1
        matrix.asc_output_yaw['PR2','PRC2'] = 1
        #matrix.asc_output_pit['IM3','INP1'] = 1
        #matrix.asc_output_yaw['IM3','INP1'] = 1

        #Turn off all dof bank filters
        #for dof in ['INP1','INP2','PRC2']:
        for dof in ['INP2','PRC2']:
            ezca.switch('ASC-'+dof+'_P','FMALL','INPUT','OFF','OUTPUT','ON')
            ezca.switch('ASC-'+dof+'_Y','FMALL','INPUT','OFF','OUTPUT','ON')

        # Set INP2 filters
        ezca.switch('ASC-INP2_P','FM6','FM7','ON')
        ezca.switch('ASC-INP2_Y','FM6','FM7','ON')
        # Set PRC2 filters (try without FM7 int, makes handling easier)
        ezca.switch('ASC-PRC2_P','FM4','FM5','FM8','ON')
        ezca.switch('ASC-PRC2_Y','FM4','FM5','FM8','ON')
        # Set INP1 filters
        #ezca.switch('ASC-INP1_P','FM1','FM2','ON')
        #ezca.switch('ASC-INP1_Y','FM1','FM2','ON')

        #Set the PRC1 gains (sets how fast the PRM is aligned)
        ezca['ASC-INP2_P_GAIN'] = -20	#-60	#Reduced by 1/3 to compensate for WFS rephasing
        ezca['ASC-INP2_Y_GAIN'] = 20	#60
        ezca['ASC-PRC2_P_GAIN'] = -1000	#-3000
        ezca['ASC-PRC2_Y_GAIN'] = -1000	#-3000
        #ezca['ASC-INP1_P_GAIN'] = 0.5
        #ezca['ASC-INP1_Y_GAIN'] = 0.5

        time.sleep(2)

        #Turn on servos
        #for dof in ['INP1','INP2','PRC2']:
        for dof in ['INP2','PRC2']:
            ezca.switch('ASC-'+dof+'_P','INPUT','ON')
            ezca.switch('ASC-'+dof+'_Y','INPUT','ON')

        self.err_chans = []
        for pd in ['A','B']:
            for dof in ['PIT','YAW']:
                 self.err_chans.append('ASC-REFL_{}_RF9_I_{}_OUTPUT'.format(pd,dof))
        #for dof in ['PIT','YAW']:
        #    self.err_chans.append('IMC-IM4_TRANS_{}_OUTPUT'.format(dof))
        self.timer['wait'] = 45

        self.irarmascAvgs = EzAvg(ezca,30,self.err_chans)

    #@assert_reflwfscentered
    @assert_arm_locked
    def run(self):

        [done,errs] = self.irarmascAvgs.ezAvg()
        if not done:
            notify('Filling EzAvg buffer')
            return

        #errs = cdsutils.avg(2,self.err_chans)

        reflwfsA_pit = errs[0]
        reflwfsA_yaw = errs[1]
        reflwfsB_pit = errs[2]
        reflwfsB_yaw = errs[3]

        p_in = ezca['IMC-IM4_TRANS_SUM_OUTPUT']

        # Changed yaw thresholds from 1.0 to 0.3, put from 1.0 to 0.6 ajm210205
        if abs(reflwfsA_pit)>2.0 or abs(reflwfsA_yaw)>1.0 or abs(reflwfsB_pit)>2.0 or abs(reflwfsB_yaw)>1.0:
            notify('waiting for WFS signals to zero')
            #log('apit is '+str(reflwfsA_pit)+', bpit is '+str(reflwfsB_pit))
            #log('ayaw is '+str(reflwfsA_yaw)+', byaw is '+str(reflwfsB_yaw))
            return

        if not self.timer['wait']:
            return

        return True

#------------------------------------------------------------------------------
# Offload control signals to the alignment sliders.
class OFFLOAD_INPUT_ASC_CONTROL(GuardState):
    request = False
    index = 19

    @assert_arm_locked
    def main(self):

        self.offload_list = ['IM3','IM4','PR2']

        # Time to offload
        t_offload = 10
        
        for optic in self.offload_list:
        #for optic in ['IM4','PR2']:
            for dof in ['P','Y']:
                ezca['SUS-'+optic+'_M1_OPTICALIGN_'+dof+'_TRAMP'] = t_offload

        # Offload the control signal to the suspension
        chans = []
        for optic in self.offload_list:
        #for optic in ['IM4','PR2']:
            for dof in ['P','Y']:
                chans.append("SUS-{}_M1_LOCK_{}_OUTPUT".format(optic,dof))
                #cal = ezca['SUS-'+optic+'_M1_OPTICALIGN_'+dof+'_GAIN']
                #offload = cdsutils.avg(-5,'SUS-'+optic+'_M1_LOCK_'+dof+'_OUTPUT')
                #ezca['SUS-'+optic+'_M1_OPTICALIGN_'+dof+'_OFFSET'] += offload/cal


        offloads = ezavg(ezca,10,chans)
        #offloads = cdsutils.avg(5,chans)
        j = 0
        for chan in chans:
            log("{} offload is {}".format(chan,offloads[j]))
            j+=1

        i = 0
        for optic in self.offload_list:
        #for optic in ['IM4','PR2']:
            for dof in ['P','Y']:
                cal = ezca['SUS-'+optic+'_M1_OPTICALIGN_'+dof+'_GAIN']
                ezca['SUS-'+optic+'_M1_OPTICALIGN_'+dof+'_OFFSET'] += offloads[i]/cal
                i+=1

        self.timer['offload'] = t_offload

    @assert_arm_locked
    def run(self):

        if not self.timer['offload']:
            return
        # Do some checks, are we still locked?
        return True

#------------------------------------------------------------------------------
Sens_Mtrx = {
             "P": np.matrix([[-64.7,34.0],[-73.1,18.0]]),
             "Y": np.matrix([[69.6,-23.4],[-71.6,2.9]])
}

class REFLWFS_TO_TESTMASSES_SERVOS(GuardState):
    request = False
    index = 21

    #@assert_reflwfscentered
    @assert_arm_locked
    def main(self):

        # Set the input matrix
        for dof in ['DHARD','CHARD']:
            matrix.asc_input_pit.zero(row=dof)
            matrix.asc_input_yaw.zero(row=dof)

        matrix.asc_input_pit['CHARD','REFL_A_RF9_I'] = Sens_Mtrx["P"][0,0]
        matrix.asc_input_pit['CHARD','REFL_B_RF9_I'] = Sens_Mtrx["P"][0,1]
        matrix.asc_input_pit['DHARD','REFL_A_RF9_I'] = Sens_Mtrx["P"][1,0]
        matrix.asc_input_pit['DHARD','REFL_B_RF9_I'] = Sens_Mtrx["P"][1,1]

        matrix.asc_input_yaw['CHARD','REFL_A_RF9_I'] = Sens_Mtrx["Y"][0,0]
        matrix.asc_input_yaw['CHARD','REFL_B_RF9_I'] = Sens_Mtrx["Y"][0,1]
        matrix.asc_input_yaw['DHARD','REFL_A_RF9_I'] = Sens_Mtrx["Y"][1,0]
        matrix.asc_input_yaw['DHARD','REFL_B_RF9_I'] = Sens_Mtrx["Y"][1,1]

        #Set the output matres
        for dof in ['DHARD','CHARD']:
            matrix.asc_output_pit.zero(col=dof)
            matrix.asc_output_yaw.zero(col=dof)

        #TODO: work this out:
        matrix.asc_output_pit['ITM'+arm,'CHARD'] = 1
        matrix.asc_output_yaw['ITM'+arm,'CHARD'] = 1
        matrix.asc_output_pit['ETM'+arm,'DHARD'] = 1
        matrix.asc_output_yaw['ETM'+arm,'DHARD'] = 1

        #Turn off all dof bank filters
        for dof in ['DHARD','CHARD']:
            ezca.switch('ASC-'+dof+'_P','FMALL','INPUT','OFF','OUTPUT','ON')
            ezca.switch('ASC-'+dof+'_Y','FMALL','INPUT','OFF','OUTPUT','ON')

        # Set DHARD filters (no int makes offloading simpler)
        for dof in ['DHARD','CHARD']:
            ezca.switch('ASC-'+dof+'_P','FM7','ON')
            ezca.switch('ASC-'+dof+'_Y','FM7','ON')

        # Set the pre gains
        ezca['ASC-DHARD_P_PRE_GAIN'] = 1
        ezca['ASC-DHARD_Y_PRE_GAIN'] = 1
        ezca['ASC-CHARD_P_PRE_GAIN'] = 1
        ezca['ASC-CHARD_Y_PRE_GAIN'] = 1

        #Set the HARD gains (sets how fast the Test Masses are aligned) #TODO: Maybe increase, kinda slow
        ezca['ASC-DHARD_P_GAIN'] = 0.1
        ezca['ASC-DHARD_Y_GAIN'] = 0.1
        ezca['ASC-CHARD_P_GAIN'] = 0.1
        ezca['ASC-CHARD_Y_GAIN'] = 0.1

        time.sleep(2)

        self.err_chans = []
        for pd in ['A','B']:
            for dof in ['PIT','YAW']:
                 self.err_chans.append('ASC-REFL_{}_RF9_I_{}_OUTPUT'.format(pd,dof))

        #Turn on servos
        #for dof in ['DHARD','CHARD']:
        #    ezca.switch('ASC-'+dof+'_P','INPUT','ON')
        #    ezca.switch('ASC-'+dof+'_Y','INPUT','ON')

        self.irarmascAvgs = EzAvg(ezca,30,self.err_chans)

        self.timer['wait'] = 45

    #@assert_reflwfscentered
    @assert_arm_locked
    def run(self):
        # FIXME: Wait until signals WFS signals go to zero and AS-C_SUM is max
        # For now just wait some time
        # abs of WFS signals less than 1, ASC_SUM eq to 325

        [done,errs] = self.irarmascAvgs.ezAvg()
        if not done:
            notify('Filling up EzAvg buffer')
            return

        #errs = cdsutils.avg(3,self.err_chans)

        reflwfsA_pit = errs[0]
        reflwfsA_yaw = errs[1]
        reflwfsB_pit = errs[2]
        reflwfsB_yaw = errs[3]

        p_in = ezca['IMC-IM4_TRANS_SUM_OUTPUT']

        if abs(reflwfsA_pit)>1.0 or abs(reflwfsA_yaw)>1.0 or abs(reflwfsB_pit)>1.0 or abs(reflwfsB_yaw)>1.0:
            notify('waiting for WFS signals to zero')
            #log('apit is '+str(reflwfsA_pit)+', bpit is '+str(reflwfsB_pit))
            #log('ayaw is '+str(reflwfsA_yaw)+', byaw is '+str(reflwfsB_yaw))
            return

        if not self.timer['wait']:
            return

        return True

#------------------------------------------------------------------------------
'''
# Offload control signals to the alignment sliders.
class OFFLOAD_TESTMASS_ASC_CONTROL(GuardState):
    request = False
    index = 22

    @assert_arm_locked
    def main(self):

        # Time to offload
        t_offload = 10
        
        for optic in ['ITMX','ETMX']:
            for dof in ['P','Y']:
                ezca['SUS-'+optic+'_M0_OPTICALIGN_'+dof+'_TRAMP'] = t_offload

        # Offload the control signal to the suspension
        #TODO: find calibration
        cal = {'P': 21.31*, 'Y': 45.90*}
        for optic in ['ITMX','ETMX']:
            for dof in ['P','Y']: 
                offload = cdsutils.avg(-5,'SUS-'+optic+'_L1_LOCK_'+dof+'_OUTPUT')
                ezca['SUS-'+optic+'_M0_OPTICALIGN_'+dof+'_OFFSET'] += offload/cal[dof]

        self.timer['offload'] = t_offload

    @assert_arm_locked
    def run(self):

        if not self.timer['offload']:
            return
        # Do some checks, are we still locked?
        return True
'''
#------------------------------------------------------------------------------

# Aligned state - Ensures that the alignment procedure has been
# successful.
class IR_CAV_ALIGNED(GuardState):
    
    # Requestable.
    request = True
    
    # The penultimate step.
    index = 20
    
    
    # Main class method - Disable WFS centring and enable monitoring of
    # alignment.
    def main(self):
        
        # Save the offsets on IM3.
        align_save("IM3", ezca)

        # Save the offsets on IM4.
        align_save("IM4", ezca)
        
        # Save the offsets on PR2.
        align_save("PR2", ezca)

        # Save the offsets on IM4.
        #align_save("IM3", ezca)
        
        # TODO: Enable monitoring of IRX alignment, and lock status.
        
        # Flag for the first iteration of run.
        self.first_run = True
        
        # Timer to record some data.
        self.timer["record"] = 6
    
    
    # Run class method - Monitor alignment of IRX cavity (likely
    # ensure that power doesn't drop too low, and error signals are
    # close enough to zero).
    def run(self):
        # TODO: Error checks, such as is IRX still locked.
        
        
        # TODO: Monitor alignment.
        
        # Check to see if this is the first pass of run.
        #if self.first_run:
        #    # Set the first run flag to false, for subsequent runs.
        #    self.first_run = False
        #    
        #    # Generate, and log, the header for the saved data.
        #    header = "TRANS, ERR, PR2_P_ERR, PR2_Y_ERR, IM4_P_ERR, IM4_Y_ERR"
        #    log(header)
        
        # Format the data for the log.
        data = "{0:.3e},".format(ezca.read(":LSC-TR_X_NORM_INMON")) + \
               "{0:.3e},".format(ezca.read(":LSC-YARM_INMON")) + \
               "{0:.3e},".format(ezca.read(":ASC-REFL_B_RF9_I_PIT_OUTMON")) + \
               "{0:.3e},".format(ezca.read(":ASC-REFL_B_RF9_I_YAW_OUTMON")) + \
               "{0:.3e},".format(ezca.read(":ASC-REFL_A_RF9_I_PIT_OUTMON")) + \
               "{0:.3e}".format(ezca.read(":ASC-REFL_A_RF9_I_YAW_OUTMON"))
        
        # Record for a set amount of time.
        #if not self.timer["record"]:
        #    # Save the information to the guardian log.
        #    log(data)
        
        # For now succeed.
        return True


#------------------------------------------------------------------------------

# Manual Alignment state - Allow the operator to maunally align the
# IM4 and PR2 optics.
class IR_MANUAL_ALIGN(GuardState):
    request = False
    index = 16
    
    
    # Main class method - Switch off the WFS centring, and enable
    # monitoring for returning to automatic alignment. 
    def main(self):
        # Switch off the REFL WFS centring.
        turn_off_dc_centering()
        
        
        # TODO: Setup for monitoring IRX lock status.
        
        
        # TODO: Setup for monitoring manual alignment process.
        
        # Flag for checking the first run of the run method.
        self.first_run = True
        
        
        # Set a timer to remind the operator every 15 seconds.
        self.timer["remind"] = 15
    
    
    # Run class method - Monitor the manual alignment, and transition
    # to automatic alignment were appropriate.
    def run(self):
        # TODO: Ensure that IRX is still locked. If not return to the
        # error state.
        
        
        # Check for reminder timer expiration
        if self.timer["remind"]:
            # Remind the operator
            log("IM4 and PR2 are in manual alignment, please manually align.")
            notify("IM4 and PR2 are in manual alignment, please manually " + \
                   "align.")
            
            
            # Reset the timer.
            self.timer["remind"] = 15
        
        
        # TODO: Decide when it can switch back to automatic alignment.
        
        # Check to see if this is the first pass of run.
        if self.first_run:
            # Set the first run flag to false, for subsequent runs.
            self.first_run = False
            
            # Generate, and log, the header for the saved data.
            header = "TRANS, ERR"
            log(header)
        
        # Format the data for the log.
        data = "{0:.3e}, {1:.3e}".format(ezca.read(":LSC-TR_X_NORM_INMON"),
                                         ezca.read(":LSC-YARM_INMON"))
        
        # Save the information to the guardian log.
        #log(data)


#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
#                                    Edges
#------------------------------------------------------------------------------

# Allowable transitions.
edges = [
         # Recovery.
         ("INIT", "IR_ALIGN_IDLE"),
         ("IR_ALIGN_RESET", "IR_ALIGN_IDLE"),
         # Xarm process.
         ("IR_ALIGN_IDLE", "IM4_TRANS_CENTERING"),
         #("IRX_ALIGN_IDLE", "MISALIGN_FOR_IRX_CAV"),
         ("IM4_TRANS_CENTERING","MISALIGN_FOR_IR_CAV"),
         ("MISALIGN_FOR_IR_CAV", "LOCK_IR_CAV"),
         ("SET_POWER_TO_10W", "LOCK_IR_CAV"),
         ("LOCK_IR_CAV", "IR_CAV_LOCKED"),
         ("IR_CAV_LOCKED", "REFLWFS_DC_CENTERING"),
         ("REFLWFS_DC_CENTERING", "REFLWFS_TO_INPUT_SERVOS"),
         ("REFLWFS_TO_INPUT_SERVOS", "OFFLOAD_INPUT_ASC_CONTROL"),
         ("OFFLOAD_INPUT_ASC_CONTROL", "IR_CAV_ALIGNED"),
         ("REFLWFS_DC_CENTERING", "REFLWFS_TO_TESTMASSES_SERVOS"),
         ("IR_MANUAL_ALIGN", "REFLWFS_DC_CENTERING"),
        ]


# END.
