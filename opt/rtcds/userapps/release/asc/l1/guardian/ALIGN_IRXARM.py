# Docstring:
"""
Specialisation of the ALIGN_IRARM guardian script for the X arm.

Author: Nathan Holland, A. Mullavey.
Date: 2019-05-07
Contact: adam.mullavey@ligo.org

"""

# Import the base X/Y code
import ALIGN_IRARM

ALIGN_IRARM.arm = 'X'

from ALIGN_IRARM import *
